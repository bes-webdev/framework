<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 8/10/19
 * Time: 11:10
 */

namespace App\Listeners;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SubscriberEventExemple implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * SubscriberEventExemple constructor.
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            'exemple' => [
                ['doSomething', 0]

            ]
        ];
    }

    public function doSomething(MyEvent $event)
    {
        $this->log->critical("sleeping students : " . implode('|', $event->getSleepingStudents()));
    }
}
