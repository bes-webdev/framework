<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 22/10/19
 * Time: 12:03
 */

namespace App\Listeners\doctrine;

use App\Entity\BlogPost;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping\Annotations as ORM;

class BlogPostDoctrineListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof BlogPost) {
            $entity->setDate(new \DateTime());
        }
    }
}
