<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 8/10/19
 * Time: 11:13
 */

namespace App\Listeners;

use Symfony\Component\EventDispatcher\Event;

class MyEvent extends Event
{
    private $sleepingStudents = [];

    /**
     * MyEvent constructor.
     * @param array $sleepingStudents
     */
    public function __construct(array $sleepingStudents)
    {
        $this->sleepingStudents = $sleepingStudents;
    }

    /**
     * @return array
     */
    public function getSleepingStudents(): array
    {
        return $this->sleepingStudents;
    }
}
