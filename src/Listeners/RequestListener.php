<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 8/10/19
 * Time: 10:54
 */

namespace App\Listeners;

use App\Services\AdminMailer;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener
{

    /**
     * @var AdminMailer
     */
    private $mailer;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * RequestListener constructor.
     * @param AdminMailer $mailer
     * @param Logger $log
     */
    public function __construct(AdminMailer $mailer, LoggerInterface $log)
    {
        $this->mailer = $mailer;
        $this->log = $log;
    }


    public function onKernelException(ExceptionEvent $event)
    {
        $this->log->addInfo($event->getException()->getCode());
        $this->mailer->sendAdminEmail();
    }
}
