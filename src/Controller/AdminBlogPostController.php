<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 15/10/19
 * Time: 9:31
 */

namespace App\Controller;

use App\Entity\BlogPost;
use App\Form\BlogPostType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilder;

/**
 * Class AdminBlogPostController
 * @package App\Controller
 * @Route("/admin/blogPost")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminBlogPostController extends AbstractController
{

    /**
     * @Route("", name="addBlogPost", methods={"GET","POST"})
     */
    public function addBlogPost(Request $request, EntityManagerInterface $em)
    {
        $blogPost = new BlogPost();
        $blogPost->setAuthor("Marty Mac Fly");
        $blogPost->setTitle("Comme ça");
        $blogPost->setContent("le content du blog Post va se retrouver dans le form");

        $form = $this->createForm(BlogPostType::class, $blogPost);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($blogPost);
            $em->flush();
            return $this->redirectToRoute('blogPost', ['id' => $blogPost->getId()]);
        }



        return $this->render('/admin/blog-post-edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}", name="editBlogPost", methods={"GET","POST"})
     */
    public function editBlogPost(BlogPost $blogPost, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(BlogPostType::class, $blogPost);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($blogPost);
            $em->flush();
            return $this->redirectToRoute('blogPost', ['id' => $blogPost->getId()]);
        }



        return $this->render('/admin/blog-post-edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name="removeBlogPost", methods={"GET"})
     */
    public function removeBlogPost(BlogPost $blogPost, EntityManagerInterface $em): RedirectResponse
    {
        $em->remove($blogPost);
        $em->flush();

        return $this->redirectToRoute("home");
    }
}
