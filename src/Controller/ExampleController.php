<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExampleController extends AbstractController
{

    /**
     * @Route("/example",name="exampleRoute")
     */
    public function homeExemple()
    {
        return new Response("exampleRoute");
    }
}
