<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 1/10/19
 * Time: 12:20
 */

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Tag;
use App\Listeners\MyEvent;
use App\Repository\BlogPostRepository;
use App\Repository\CategoryRepository;
use App\Repository\TagRepository;
use App\Services\AdminMailer;
use App\Services\MyCustomService;
use PhpParser\Node\Scalar\String_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class HelloController extends AbstractController
{

    /**
     * @Route("/",name="home")
     */
    public function home(BlogPostRepository $blogPostRepository)
    {
        $posts = $blogPostRepository->findAllWithTags();
        dump($posts);
        return $this->render('pages/home.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route("/hello/{name}",name="hello")
     */
    public function hello(String $name)
    {
        return $this->render('hello.html.twig', ["name" => '<h2>'.$name.'</h2>']);
    }


    /**
     * @Route("/post/{id<\d+>}",name="blogPost", methods={"POST","GET"})
     */
    public function blogPost(BlogPost $blogPost)
    {
        return $this->render('pages/post.html.twig', ['post' => $blogPost]);
    }

    /**
    * @Route("/category/{id<\d+>}",name="category", methods={"GET"})
    */
    // Ici je demande à Symfony de me fournir le repository des categories
    public function category(int $id, CategoryRepository $repository)
    {
        $category = $repository->find($id);

        return $this->render('pages/category.html.twig', ['category' => $category]);
    }

    /**
     * @Route("/tag/{id<\d+>}",name="tag", methods={"GET"})
     */
    // Ici symfony à compris que je voulais un tag correspondant à l'id donné dans la route
    public function tag(Tag $tag, BlogPostRepository $blogPostRepository, Request $request)
    {
        $pagination = $blogPostRepository->findByTags([$tag], $request->query->get('page') ? $request->query->get('page') : 1);
        return $this->render('pages/tag.html.twig', ['tag' => $tag, 'posts'=> $pagination]);
    }


    /**
     * @Route("/contact",name="contact")
     */
    public function contact()
    {
        return $this->render('pages/contact.html.twig');
    }



    /**
     * @Route("/event",name="eventExemple", methods={"GET"})
     */
    public function eventExemple(EventDispatcherInterface $eventDispatcher)
    {
        $eventDispatcher->dispatch("exemple", new MyEvent(["thib","Tom"]));



        return new Response("event should be dispatched");
    }
}
