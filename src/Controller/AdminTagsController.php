<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 17/10/19
 * Time: 11:30
 */

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Tag;
use App\Form\CategoryType;
use App\Form\TagType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminTagsController extends AbstractController
{

    /**
     * @Route("/admin/tag",name="createTag")
     * @param Request $request
     * @param EntityManager $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createTag(Request $request, EntityManagerInterface $em, FormFactoryInterface $factory)
    {
        $tag = new Tag();

        $form = $factory->create(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($tag);
            $em->flush();
            return $this->redirectToRoute("tag", ['id'=> $tag->getId()]);
        }

        return $this->render('/admin/blog-post-edit.html.twig', ['form'=> $form->createView()]);
    }
}
