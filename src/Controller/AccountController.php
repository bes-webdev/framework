<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 7/11/19
 * Time: 10:23
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\ProfileType;
use App\Security\model\ChangePasswordModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AccountController
 * @package App\Controller
 * @Route("/account")
 */
class AccountController extends AbstractController
{

    /**
     * @Route("/profile",name="profile")
     */
    public function profile(Request $request, EntityManagerInterface $entityManager)
    {
        $user = $this->getUser();

        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();
            $this->redirectToRoute("profile");
        }

        return $this->render("pages/profile.html.twig", ['form'=>$form->createView()]);
    }

    /**
     * @Route("/change-password", name="changePassword")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $changePasswordInfos = new ChangePasswordModel();
        $form = $this->createForm(ChangePasswordType::class, $changePasswordInfos);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && $passwordEncoder->isPasswordValid($user, $changePasswordInfos->getOldPassword())) {
            $user->setPassword($passwordEncoder->encodePassword($user, $changePasswordInfos->getNewPassword()));
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Well done, you got a brand new password. Keep it safe !'
            );

            return $this->redirectToRoute('changePassword');
        } elseif ($form->isSubmitted() && !$passwordEncoder->isPasswordValid($user, $changePasswordInfos->getOldPassword())) {
            $formError = new FormError("The old password is incorrect");
            $form->get('oldPassword')->addError($formError);
        }
        return $this->render("pages/change-password.html.twig", ['form'=>$form->createView()]);
    }
}
