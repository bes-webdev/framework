<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 3/10/19
 * Time: 10:20
 */

namespace App\Services;

class MyCustomService
{
    public function isItCool($var)
    {
        return rand(0, 1) === 0;
    }
}
