<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 3/10/19
 * Time: 11:41
 */

namespace App\Services;

class AdminMailer
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var String
     */
    private $adminEmail;

    /**
     * AdminMailer constructor.
     * @param \Swift_Mailer $mailer
     * @param String $adminEmail
     */
    public function __construct(\Swift_Mailer $mailer, String $adminEmail)
    {
        $this->mailer = $mailer;
        $this->adminEmail = $adminEmail;
    }



    public function sendAdminEmail()
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('demo@bes-webdeveloper-seraing.be')
            ->setTo($this->adminEmail)
            ->setBody(
                "ok",
                'text/plain'
            )
        ;

        $this->mailer->send($message);
    }
}
