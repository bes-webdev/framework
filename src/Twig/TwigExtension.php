<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    /**
     * retourne la liste des filtres
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'filterPrice']),
        ];
    }

    public function getFunctions()
    {
        return [
            // appellera la fonction LipsumGenerator:generate()
            new TwigFunction('lipsum', [$this, 'lispumGenerate']),
        ];
    }

    public function filterPrice(Float $priceValue) : String
    {
        return $priceValue.' €';
    }

    /**
     * @param int $length
     * @return String
     */
    public function lispumGenerate(int $length) : String
    {
        $lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." .
            " Etiam pretium dictum massa vitae v" .
            "enenatis. Duis accumsan et nunc et sag" .
            "ittis. Donec sit amet iaculis mauris. Ut bibend" .
            "um ex in eros pellentesque, condimentum solli" .
            "citudin elit consequat" .
            ". Donec " .
            "vitae augue fermentum enim suscipit lobortis quis eu enim. Praesent " .
            "viverra scelerisque" .
            " urna at pretium. Cras et ultrices magna, non consequat tortor";
        return substr($lorem, 0, $length);
    }
}
