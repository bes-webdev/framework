<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 7/11/19
 * Time: 11:43
 */

namespace App\Security\model;

use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;

class ChangePasswordModel
{
    /**
     * @var string
     */
    private $oldPassword;
    /**
     * @var string
     * @Assert\Length(min="8")
     */
    private $newPassword;

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     */
    public function setOldPassword($oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     */
    public function setNewPassword($newPassword): void
    {
        $this->newPassword = $newPassword;
    }

    /**
     * @return bool
     * @Assert\IsTrue(message="The new password cannot be your old password. It's dumb.")
     * Ceci est une validation au niveau de la classe
     */
    public function isPasswordNew() : Boolean
    {
        return $this->getNewPassword() != $this->getOldPassword();
    }
}
