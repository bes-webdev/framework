<?php

namespace App\Form;

use App\Entity\BlogPost;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', null, ['label'=>false, 'attr'=> ['placeholder' => 'this is the riddim of the night']])
            ->add('title')
            ->add('content')
            ->add('published')
            // Comme je ne précise pas le type il va chercher EntityType tout seul
            ->add('category')
            // Je construit un entityType à la main, ça fait +- comme pour category mais je peux customiser le type d'input (checkbox, radio, select,..)
            // Comme je n'ai pas de __toString dans Tag, je précise le choice_label ici en donnant un nom de propriété de tag à utiliser pour l'affichage
            ->add('tags', EntityType::class, ['class' => Tag::class, 'choice_label' => "name",'multiple' => true, 'expanded' => true])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" =>  BlogPost::class
        ]);
    }
}
