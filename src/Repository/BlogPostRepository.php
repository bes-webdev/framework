<?php

namespace App\Repository;

use App\Entity\BlogPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use PhpParser\Node\Expr\Array_;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends ServiceEntityRepository
{

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    const PAGE_SIZE = 3;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, BlogPost::class);
        $this->paginator = $paginator;
    }

    /**
     * @return BlogPost[]
     */
    public function findAllWithTags()
    {
        return  $this->createQueryBuilder('b')// Alias (b) va représenter l'entité
            ->leftJoin('b.tags', 'tags')
            ->addSelect('tags')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param $tags
     * @return BlogPost
     */
    public function findByTags($tags, $page = 1): PaginationInterface
    {
        $query =  $this->createQueryBuilder('b')
            ->leftJoin('b.tags', 'tags')
            ->where('tags in (:tags)')
            ->setParameter('tags', $tags)
            ->getQuery();

        return $this->paginator->paginate($query, $page, 2);
    }

    // /**
    //  * @return BlogPost[] Returns an array of BlogPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlogPost
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
