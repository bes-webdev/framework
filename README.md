# Demo du cours de Symfony 4

## Console
pour afficher la liste des commandes `php bin/console`

## Doctrine

### Paramètres
ils se trouvent dans config/packages/doctrine.yaml, et le param (entre %) qui désigne la DB et son chemin est dans le fichier .env

### Générer & mettre à jour des entités 
``php bin/console make:entity``

### Créer un schema
```php bin/console doctrine:schema:create```

### Modifier un schema
```php bin/console doctrine:schema:update --dump-sql``` pour afficher les requetes qui seront effectuées

```php bin/console doctrine:schema:update --force``` pour executer les requetes de mise à jour de schema